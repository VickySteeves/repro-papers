# Writing reprocible geoscience papers using R Markdown, Docker, and GitLab

This repository contains the course material for the EGU General Assembly 2018 short course _"Writing reprocible geoscience papers using R Markdown, Docker, and GitLab"_.
It is co-organised by [Daniel Nüst](https://orcid.org/0000-0002-0024-5046), [Vicky Steeves](https://orcid.org/0000-0003-4298-168X), [Markus Konkol](http://orcid.org/0000-0001-6651-0976), and [Edzer Pebesma](http://orcid.org/0000-0001-8049-7069).

## Internals

This course website is based on the [**bookdown** package](https://bookdown.org), built automatically with the GitLab CI (see file `.gitlab-ci.yml` and work by [Romain Lesur](https://rlesur.gitlab.io/bookdown-gitlab-pages) and available online in various formats: LINK HERE

You can build the page and all output formats locally with

```{r eval=FALSE}
install.packages("bookdown")
library("bookdown")
bookdown::render_book("index.Rmd", "all", output_dir = "public")
```

Or just use the "Build" > "Build Book" - Button in RStudio.

## Structure

Instructions partly based on [A minimal book example using bookdown](https://github.com/rstudio/bookdown-demo). 

- Each Rmd file contains one and only one chapter, and a chapter is defined by the first-level heading `#`.
- Each sentence is on a seperate line to improve git merges and change tracking.
- `packages.bib` includes a bib database for R packages; it can be created with the command below.
```{r include=FALSE}
knitr::write_bib(c(
  .packages(), 'bookdown', 'knitr', 'rmarkdown', 'rticles'
), 'packages.bib')
```
- Write citations for packages, e.g. `[@R-bookdown]` and papers, e.g. `[@xie2015]`.
- Label chapters and sections using `{#label}` in the headlines and reference as `\@ref(label)`.
