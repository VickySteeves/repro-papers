# tl;dr {#summary}

**Manage your workflows in digital notebooks**

(recommendation: R Markdown, RStudio)

<blockquote class="twitter-tweet" data-lang="de"><p lang="en" dir="ltr">When you try to replicate a paper using the methods section <a href="https://t.co/3HaFUDZogi">pic.twitter.com/3HaFUDZogi</a></p>&mdash; Sylvain ❄️👨🏻‍🎓 (@DevilleSy) <a href="https://twitter.com/DevilleSy/status/958761021421903872?ref_src=twsrc%5Etfw">31. Januar 2018</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

**Use version management tools to track the development**

(recommendation: git)

![](https://media.giphy.com/media/P07JtCEMQF9N6/giphy.gif)

**Collaborate**

(recommendation: GitLab)

![](https://i.imgur.com/bKojYDk.gif)

**Have fun with Open Science and show it**

![](https://media.giphy.com/media/ZrTCZOc2RXYVa/giphy.gif)
